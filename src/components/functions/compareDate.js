function compareDate(a, b) {
  const dateA = a.starts
  const dateB = b.starts

  let comparison = 0
  if (dateA > dateB) {
    comparison = 1
  } else if (dateA < dateB) {
    comparison = -1
  }

  return comparison
}

export default compareDate
