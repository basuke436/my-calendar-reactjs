function colorsOfTheDay(getDayValue) {
  switch (getDayValue) {
    case 0:
      return "#cc0000"

    case 1:
      return "#faad14"

    case 2:
      return "#ff33cc"

    case 3:
      return "#52c41a"

    case 4:
      return "#ff6600"

    case 5:
      return "#1890ff"

    case 6:
      return "#9900cc"

    default:
      break
  }
}

export default colorsOfTheDay
