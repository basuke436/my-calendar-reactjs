function getParseFormat(date) {
  const getDate = date
  let d = getDate.getDate()
  d = d < 10 ? `0${d}` : d

  let m = getDate.getMonth() + 1
  m = m < 10 ? `0${m}` : m

  let y = getDate.getFullYear()

  return `${y}-${m}-${d}`
}

export default getParseFormat
