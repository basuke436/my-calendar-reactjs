function getDateAndTimeFormat(date) {
  const getDate = date
  let d = getDate.getDate()
  d = d < 10 ? `0${d}` : d

  let m = getDate.getMonth() + 1
  m = m < 10 ? `0${m}` : m

  let y = getDate.getFullYear()

  let hour = getDate.getHours()
  hour = hour < 10 ? `0${hour}` : hour

  let min = getDate.getMinutes()
  min = min < 10 ? `0${min}` : min

  let sec = getDate.getSeconds()
  sec = sec < 10 ? `0${sec}` : sec

  return `${y}-${m}-${d} ${hour}:${min}:${sec}`
}

export default getDateAndTimeFormat
