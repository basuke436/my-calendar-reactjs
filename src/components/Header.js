import React, { useState, useEffect } from "react"
import { connect } from "react-redux"
import styled from "styled-components"
import { Icon } from "antd"

const HeaderContainer = styled.div`
  background-color: ${props =>
    props.theme === "sun" ? "rgb(255, 255, 255)" : "rgb(50, 50, 50)"};
  color: ${props =>
    props.theme === "sun" ? "rgb(0, 0, 0)" : "rgb(225, 225, 225)"};
  transition: 0.3s;

  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0.75rem 2rem;
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  position: relative;

  &.hidden {
    opacity: 0;
  }
`

const HeaderContent = styled.span`
  font-size: 1rem;
  margin-bottom: 0.25rem;
`

const BreadCrumb = styled.span`
  -webkit-user-select: none; /* Safari 3.1+ */
  -moz-user-select: none; /* Firefox 2+ */
  -ms-user-select: none; /* IE 10+ */
  user-select: none; /* Standard syntax */
`

const ActiveBreadCrumb = styled(BreadCrumb)`
  border-bottom: 1px solid
    ${props => (props.theme === "sun" ? "rgb(0, 0, 0)" : "rgb(225, 225, 225)")};
`

const BreadCrumbIcon = styled(Icon)`
  margin-left: 0.5rem;
  margin-right: 0.5rem;
`

function mapStateToProps(state) {
  return state
}

function Header(props) {
  const [headerContainerState, setHeaderContainerState] = useState("hidden")

  useEffect(() => {
    setTimeout(() => {
      setHeaderContainerState("animated fadeIn")
    }, 250)
  }, [])

  return (
    <HeaderContainer className={headerContainerState} theme={props.theme}>
      <HeaderContent>
        {props.breadcrumb !== undefined &&
          props.breadcrumb.map((item, index) => {
            if (index === props.breadcrumb.length - 1) {
              return (
                <ActiveBreadCrumb key={index} theme={props.theme}>
                  {item.page}
                </ActiveBreadCrumb>
              )
            }

            return (
              <BreadCrumb key={index}>
                <a href={item.url} rel="noreferrer">
                  {item.page}
                </a>
                <BreadCrumbIcon type="caret-right" />
              </BreadCrumb>
            )
          })}
      </HeaderContent>
    </HeaderContainer>
  )
}

export default connect(mapStateToProps)(Header)
