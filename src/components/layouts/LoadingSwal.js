import React from "react"
import styled from "styled-components"
import swalCustomize from "@sweetalert/with-react"
import { Icon } from "antd"

const SwalResultContainer = styled.div`
  color: ${props =>
    props.theme === "sun" ? "rgba(0, 0, 0, 0.65)" : "rgb(225, 225, 225)"};
  padding: ${props => props.padding || "0"};
  border: 0;
`

const SuccessContent = styled.p`
  font-size: 1.25rem;
  font-weight: 500;
  margin-top: 1rem;
  margin-bottom: 1rem;
`

const LoadingIcon = styled(Icon)`
  font-size: 5rem;
  margin: 1rem 0;
  color: #1890ff;
`

function LoadingSwal(title, theme) {
  return swalCustomize({
    className: theme !== "sun" && "night",
    buttons: false,
    closeOnClickOutside: false,
    closeOnEsc: false,
    content: (
      <SwalResultContainer padding="0.5rem 1rem" theme={theme}>
        <SuccessContent>{title}</SuccessContent>
        <LoadingIcon type="loading" />
      </SwalResultContainer>
    ),
  })
}

export default LoadingSwal
