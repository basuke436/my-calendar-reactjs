import React, { useState, useEffect } from "react"
import { connect } from "react-redux"
import styled from "styled-components"
import {
  format,
  addDays,
  startOfWeek,
  endOfWeek,
  startOfMonth,
  endOfMonth,
  isSameMonth,
  isSameDay,
  parse,
  addMonths,
  subMonths,
} from "date-fns"
import { Icon } from "antd"
import moment from "moment"
import getDayString from "./functions/getDayString"
import getParseFormat from "./functions/getParseFormat"
import compareDate from "./functions/compareDate"

const MyCalendar = styled.div`
  display: block;
  position: relative;
  width: 100%;
  background: var(--neutral-color);
  border: 1px solid var(--border-color);
  border-radius: 5px;
  overflow: overlay;

  .row {
    margin: 0;
    padding: 0;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    width: 100%;
  }

  .row-middle {
    align-items: center;
  }

  .col {
    flex-grow: 1;
    flex-basis: 0;
    max-width: 100%;
  }

  .col-start {
    justify-content: flex-start;
    text-align: left;
  }

  .col-center {
    justify-content: center;
    text-align: center;
    white-space: nowrap;
  }

  .col-end {
    justify-content: flex-end;
    text-align: right;
  }

  .header {
    text-transform: uppercase;
    font-weight: 700;
    font-size: 115%;
    padding: 1.5em 0;
    border-bottom: 1px solid var(--border-color);

    .icon {
      cursor: pointer;
      transition: 0.15s ease-out;

      &:hover {
        transform: scale(1.75);
        transition: 0.25s ease-out;
        color: var(--main-color);
      }

      &:first-of-type {
        margin-left: 1em;
      }

      &:last-of-type {
        margin-right: 1em;
      }
    }
  }

  .days {
    text-transform: uppercase;
    font-weight: 400;
    color: var(--text-color-light);
    font-size: 70%;
    padding: 0.75em 0;
    border-bottom: 1px solid var(--border-color);
  }

  .body {
    .cell {
      position: relative;
      height: 5em;
      border-right: 1px solid var(--border-color);
      overflow: hidden;
      cursor: pointer;
      background: var(--neutral-color);
      transition: 0.25s ease-out;

      .number {
        position: absolute;
        font-size: 82.5%;
        line-height: 1;
        top: 0.75em;
        right: 0.75em;
        font-weight: 700;
      }

      .more {
        position: absolute;
        font-size: 82.5%;
        line-height: 1;
        bottom: 0.2em;
        left: 1.2em;
        white-space: nowrap;
      }

      .bg {
        font-weight: 700;
        line-height: 1;
        color: var(--main-color);
        opacity: 0;
        font-size: 8em;
        position: absolute;
        top: -0.2em;
        right: -0.05em;
        transition: 0.25s ease-out;
        letter-spacing: -0.07em;
      }

      &:hover {
        background: var(--bg-color);
        transition: 0.5s ease-out;
      }

      &:last-child {
        border-right: none;
      }
    }

    .selected {
      border-left: 10px solid transparent;
      border-image: linear-gradient(45deg, #1a8fff 0%, #53cbf1 40%);
      border-image-slice: 1;

      .bg {
        opacity: 0.05;
        transition: 0.5s ease-in;
      }
    }

    .today {
      border-left: 10px solid transparent;
      border-image: linear-gradient(45deg, #cc0000 0%, #ee3355 40%);
      border-image-slice: 1;

      .bg {
        color: #cc0000;
      }
    }

    .has-events {
      display: flex;
      justify-content: flex-start;
      align-items: center;
    }

    .row {
      border-bottom: 1px solid var(--border-color);

      &:last-child {
        border-bottom: none;
      }
    }

    .col {
      flex-grow: 0;
      flex-basis: calc(100% / 7);
      width: calc(100% / 7);
    }

    .disabled {
      color: var(--text-color-light);
      pointer-events: none;
    }
  }

  .icon {
    font-style: normal;
    display: inline-block;
    vertical-align: middle;
    line-height: 1;
    text-transform: none;
    letter-spacing: normal;
    word-wrap: normal;
    white-space: nowrap;
    direction: ltr;

    -webkit-font-smoothing: antialiased;
    text-rendering: optimizeLegibility;
    -moz-osx-font-smoothing: grayscale;
    font-feature-settings: "liga";
  }
`

const EventOnCalendar = styled.span`
  width: 100%;
  text-align: left;
  padding: 0 15px;
  background-color: ${props => props.setBgColor || "#EFEFEF"};
  white-space: nowrap;
`

function mapStateToProps(state) {
  return state
}

function Calendar(props) {
  const [header, setHeader] = useState("")
  const [cells, setCells] = useState("")

  useEffect(() => {
    const today = new Date()
    onDateClick(parse(getParseFormat(today), "yyyy-MM-dd", today))
    fetchCalendar()
  }, [])

  useEffect(() => {
    fetchCalendar()
  }, [props.selectedDate, props.currentMonth, props.events])

  function fetchCalendar() {
    setHeader(renderHeader())
    setCells(renderCells())
  }

  function hasEvents(getDate) {
    let checkHasEvents = 0
    let eventDetailList = []
    props.events.sort(compareDate).map(item => {
      const dateTime = item.starts.split(" ")
      const date = dateTime[0]

      const date1 = getDate
      const date2 = new Date(date)

      if (
        `${date1.getFullYear()}.${date1.getMonth()}.${date1.getDate()}` ===
        `${date2.getFullYear()}.${date2.getMonth()}.${date2.getDate()}`
      ) {
        checkHasEvents = checkHasEvents + 1
        eventDetailList = [
          ...eventDetailList,
          {
            title: item.title,
            starts: item.starts,
            ends: item.ends,
            repeat: item.repeat,
            created_date: item.created_date,
          },
        ]
      }

      return 0
    })

    if (checkHasEvents > 0) {
      return {
        count: checkHasEvents,
        first: eventDetailList[0].title,
      }
    } else {
      return false
    }
  }

  function renderHeader() {
    const dateFormat = "MMMM yyyy"

    return (
      <div className="header row flex-middle">
        <div className="col col-start">
          <div className="icon" onClick={prevMonth}>
            <Icon type="left" />
          </div>
        </div>
        <div className="col col-center">
          <span>{format(props.currentMonth, dateFormat)}</span>
        </div>
        <div className="col col-end">
          <div className="icon" onClick={nextMonth}>
            <Icon type="right" />
          </div>
        </div>
      </div>
    )
  }

  function renderDays() {
    const days = []
    const stringType = window.innerWidth < 768 ? "short" : "long"

    for (let i = 0; i < 7; i++) {
      days.push(
        <div className="col col-center" key={i}>
          {getDayString(i, stringType)}
        </div>
      )
    }

    return <div className="days row">{days}</div>
  }

  function renderCells() {
    const monthStart = startOfMonth(props.currentMonth)
    const monthEnd = endOfMonth(monthStart)
    const startDate = startOfWeek(monthStart)
    const endDate = endOfWeek(monthEnd)

    const dateFormat = "d"
    const rows = []

    let days = []
    let day = startDate
    let formattedDate = ""

    while (day <= endDate) {
      for (let i = 0; i < 7; i++) {
        formattedDate = format(day, dateFormat)
        const cloneDay = day
        days.push(
          <div
            className={`col cell${
              !isSameMonth(day, monthStart)
                ? " disabled"
                : isSameDay(day, props.selectedDate)
                ? " selected"
                : ""
            }${isSameDay(day, new Date()) ? " today" : ""}${
              hasEvents(cloneDay) ? " has-events" : ""
            }`}
            key={day}
            onClick={() =>
              onDateClick(
                parse(getParseFormat(cloneDay), "yyyy-MM-dd", new Date())
              )
            }
          >
            <span className="number">{formattedDate}</span>
            <span className="bg">{formattedDate}</span>
            {hasEvents(cloneDay) && (
              <EventOnCalendar
                setBgColor={
                  !isSameMonth(day, monthStart) ? "#FAFAFA" : "#EFEFEF"
                }
              >
                {hasEvents(cloneDay).first}
              </EventOnCalendar>
            )}
            {hasEvents(cloneDay) && hasEvents(cloneDay).count > 1 ? (
              <span
                className="more"
                style={
                  !isSameMonth(day, monthStart)
                    ? { color: "rgb(225, 225, 225)" }
                    : { color: "rgb(150, 150, 150)" }
                }
              >{`${hasEvents(cloneDay).count - 1} more...`}</span>
            ) : (
              ""
            )}
          </div>
        )
        day = addDays(day, 1)
      }
      rows.push(
        <div className="row" key={day}>
          {days}
        </div>
      )
      days = []
    }

    return <div className="body">{rows}</div>
  }

  function onDateClick(day) {
    props.dispatch({
      type: "SET_SELECTED_DATE",
      data: day,
    })
    props.dispatch({
      type: "SET_START_VALUE",
      data: moment(getParseFormat(day)).add(new Date().getHours(), "h"),
    })
    props.dispatch({
      type: "SET_END_VALUE",
      data: moment(getParseFormat(day)).add(new Date().getHours() + 1, "h"),
    })
  }

  function nextMonth() {
    props.dispatch({
      type: "SET_CURRENTMONTH_DATE",
      data: addMonths(props.currentMonth, 1),
    })
  }

  function prevMonth() {
    props.dispatch({
      type: "SET_CURRENTMONTH_DATE",
      data: subMonths(props.currentMonth, 1),
    })
  }

  return (
    <MyCalendar>
      {header}
      {renderDays()}
      {cells}
    </MyCalendar>
  )
}

export default connect(mapStateToProps)(Calendar)
