import React, { useState, useEffect } from "react"
import { connect } from "react-redux"
import styled from "styled-components"
import {
  Col,
  Button,
  Icon,
  Modal,
  Input,
  Select,
  DatePicker,
  message,
  notification,
} from "antd"
import "antd/dist/antd.min.css"
import moment from "moment"
import getParseFormat from "../components/functions/getParseFormat"
import getDateAndTimeFormat from "../components/functions/getDateAndTimeFormat"
import getDayString from "../components/functions/getDayString"
import getMonthString from "../components/functions/getMonthString"
import compareDate from "../components/functions/compareDate"
import MainContainer from "../components/layouts/MainContainer"
import MainRow from "../components/layouts/MainRow"
import CardShield from "../components/layouts/CardShield"
import Card from "../components/layouts/Card"
import MainTitle from "../components/layouts/MainTitle"
import Calendar from "../components/Calendar"

const ModifiedCardShield = styled(CardShield)`
  -webkit-user-select: none; /* Safari 3.1+ */
  -moz-user-select: none; /* Firefox 2+ */
  -ms-user-select: none; /* IE 10+ */
  user-select: none; /* Standard syntax */
`

const ModifiedCard = styled(Card)`
  margin-top: 1.5rem;
`

const EventDetailBlock = styled.div`
  color: ${props =>
    props.theme === "sun" ? "rgb(100, 100, 100)" : "rgb(200, 200, 200)"};
`

const OptionBar = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 15px;

  div {
    display: flex;
    justify-content: center;
    align-items: center;

    svg {
      width: 1.5rem;
      height: 1.5rem;
      padding: 3px;
      color: ${props =>
        props.theme === "sun" ? "rgb(100, 100, 100)" : "rgb(200, 200, 200)"};
      border-radius: 30px;
      transition: 0.2s;

      &:hover {
        color: ${props => (props.theme === "sun" ? "#1890ff" : "#faad14")};
      }
    }
  }
`

const OptionIcon = styled(Icon)`
  margin-left: 15px;
  margin-right: 15px;
  cursor: pointer;
`

const AddEventsModal = styled(Modal)`
  max-width: 520px;

  div.modal-row {
    padding: 0 15px;
    margin-bottom: 20px;
  }

  .ant-modal-close-x {
    width: 45px;
    height: 45px;
    line-height: 45px;

    ${props =>
      props.settheme !== "sun" &&
      `
            color: rgb(225, 225, 225);
            &:hover {
                color: rgb(175, 175, 175);
            }
        `}
  }

  .ant-modal-content {
    background-color: ${props => props.settheme !== "sun" && "rgb(75, 75, 75)"};
    color: ${props => props.settheme !== "sun" && "rgb(225, 225, 225)"};
  }
`

const ViewEventsModal = styled(AddEventsModal)``

const EventListItem = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 10px;

  p.event-date {
    background-color: ${props =>
      props.theme === "sun" ? "#EFEFEF" : "#696969"};
    margin-bottom: 0;
  }

  div {
    display: flex;

    span.left {
      padding-right: 0.5rem;
      min-width: 180px;
    }

    span.right {
      padding-left: 0.5rem;
      border-left: 1px solid #ccc;
      white-space: nowrap;
      text-overflow: ellipsis;
      overflow: hidden;
      max-width: 280px;
    }

    span.leftBig {
      text-align: left;
      padding-right: 0.5rem;
      min-width: 220px;
      width: 220px;
    }

    span.rightBig {
      text-align: left;
      padding-left: 0.5rem;
      border-left: 1px solid #ccc;
      word-wrap: break-word;
    }
  }
`

const Title = styled.p`
  color: ${props =>
    props.theme === "sun" ? "rgb(0, 0, 0)" : "rgb(225, 225, 225)"};
  font-size: 1.2rem;
  font-weight: 500;
  margin-bottom: 5px;
`

const StyledInput = styled(Input)`
  width: 100%;
  color: ${props => props.theme !== "sun" && "rgb(255, 255, 255)"};
  background-color: ${props => props.theme !== "sun" && "rgb(90, 90, 90)"};
  border-color: ${props => props.theme !== "sun" && "rgb(125, 125, 125)"};
`

function mapStateToProps(state) {
  return state
}

function Home(props) {
  const dispatch = props.dispatch
  const [rowClass, setRowClass] = useState("hidden")
  const [addEventModalVisibility, setAddEventModalVisibility] = useState(false)
  const [viewEventModalVisibility, setViewEventModalVisibility] =
    useState(false)
  const [title, setTitle] = useState("")
  const [repeat, setRepeat] = useState("never")
  const [okBtnState, setOkBtnState] = useState({ disabled: true })
  const [eventDetail, setEventDetail] = useState([])

  useEffect(() => {
    dispatch({
      type: "SET_BREADCRUMB",
      data: [
        {
          page: "Calendar",
          url: "",
        },
      ],
    })

    const classNames = {
      first: "animated fadeIn",
    }

    setTimeout(() => {
      setRowClass(classNames.first)
    }, 150)

    message.destroy()
    notification.config({
      placement: "topRight",
      duration: 3,
    })
  }, [dispatch])

  useEffect(() => {
    if (addEventModalVisibility === false) {
      setTimeout(() => {
        setTitle("")
        setRepeat("never")
        dispatch({
          type: "SET_START_VALUE",
          data: moment(getParseFormat(new Date(props.selectedDate))).add(
            new Date().getHours(),
            "h"
          ),
        })
        dispatch({
          type: "SET_END_VALUE",
          data: moment(getParseFormat(new Date(props.selectedDate))).add(
            new Date().getHours() + 1,
            "h"
          ),
        })
      }, 100)
    }
  }, [addEventModalVisibility, dispatch, props.selectedDate])

  useEffect(() => {
    if (title !== "" && props.startValue !== null && props.endValue !== null) {
      setOkBtnState({ disabled: false })
    } else {
      setOkBtnState({ disabled: true })
    }
  }, [title, props.startValue, props.endValue])

  useEffect(() => {
    let checkHasEvents = 0
    let eventDetailList = []
    props.events.sort(compareDate).map(item => {
      const dateTime = item.starts.split(" ")
      const date = dateTime[0]

      const date1 = props.selectedDate
      const date2 = new Date(date)

      if (
        `${date1.getFullYear()}.${date1.getMonth()}.${date1.getDate()}` ===
        `${date2.getFullYear()}.${date2.getMonth()}.${date2.getDate()}`
      ) {
        checkHasEvents = checkHasEvents + 1
        eventDetailList = [
          ...eventDetailList,
          {
            title: item.title,
            starts: item.starts,
            ends: item.ends,
            repeat: item.repeat,
            created_date: item.created_date,
          },
        ]
      }

      return 0
    })

    if (checkHasEvents > 0) {
      setEventDetail(eventDetailList)
    } else {
      setEventDetail([])
    }
  }, [props.selectedDate, props.events])

  function titleHandleChange(event) {
    const value = event.target.value
    setTitle(value)
  }

  function repeatHandleChange(value) {
    setRepeat(value)
  }

  function disabledStartDate(getStartValue) {
    if (!getStartValue || !props.endValue) {
      return false
    }

    return getStartValue.valueOf() > props.endValue.valueOf()
  }

  function disabledEndDate(getEndValue) {
    if (!getEndValue || !props.startValue) {
      return false
    }

    return getEndValue.valueOf() <= props.startValue.valueOf()
  }

  function onStartChange(value) {
    if (
      value !== null &&
      props.endValue !== null &&
      value.valueOf() >= props.endValue.valueOf()
    ) {
      notification["error"]({
        message: "Warning",
        description: "The starts can not be more than or equal the ends.",
      })
    } else {
      props.dispatch({
        type: "SET_START_VALUE",
        data: value,
      })
    }
  }

  function onEndChange(value) {
    if (
      value !== null &&
      props.startValue !== null &&
      value.valueOf() <= props.startValue.valueOf()
    ) {
      notification["error"]({
        message: "Warning",
        description: "The ends can not be less than or equal to the starts.",
      })
    } else {
      props.dispatch({
        type: "SET_END_VALUE",
        data: value,
      })
    }
  }

  function goToAddAnEvent() {
    props.dispatch({
      type: "SET_EVENTS",
      data: [
        ...props.events,
        {
          title: title,
          starts: getDateAndTimeFormat(props.startValue._d),
          ends: getDateAndTimeFormat(props.endValue._d),
          repeat: repeat,
          created_date: new Date().valueOf(),
        },
      ],
    })
    setAddEventModalVisibility(false)
    message.success("An event has been added.")
  }

  function getLocaleString(getDate) {
    const dayString = getDayString(getDate.getDay())

    const year = getDate.getFullYear()
    const month = getMonthString(getDate.getMonth())
    const day = getDate.getDate()

    return `${dayString}, ${month} ${day}, ${year}`
  }

  function displayDate(getDateTime) {
    const dateTime = getDateTime
    let date = dateTime.split(" ")
    date = new Date(date[0])

    return getLocaleString(date)
  }

  function displayTime(getDateTime) {
    const dateTime = getDateTime
    let time = dateTime.split(" ")
    time = time[1].substring(0, 5)

    return time
  }

  return (
    <MainContainer className="animated fadeIn">
      <MainRow>
        <MainTitle title="Calendar" />
        <Col sm={24}>
          <ModifiedCardShield className={rowClass}>
            <OptionBar theme={props.theme}>
              <div>
                <Button
                  onClick={() => {
                    props.dispatch({
                      type: "SET_SELECTED_DATE",
                      data: new Date(),
                    })
                    props.dispatch({
                      type: "SET_CURRENTMONTH_DATE",
                      data: new Date(),
                    })
                    props.dispatch({
                      type: "SET_START_VALUE",
                      data: moment(getParseFormat(new Date())).add(
                        new Date().getHours(),
                        "h"
                      ),
                    })
                    props.dispatch({
                      type: "SET_END_VALUE",
                      data: moment(getParseFormat(new Date())).add(
                        new Date().getHours() + 1,
                        "h"
                      ),
                    })
                  }}
                >
                  Today
                </Button>
              </div>
              <div>
                <OptionIcon
                  type="bars"
                  onClick={() => setViewEventModalVisibility(true)}
                />
                <OptionIcon
                  type="plus"
                  onClick={() => setAddEventModalVisibility(true)}
                />
              </div>
            </OptionBar>
            <Calendar />
            <ModifiedCard theme={props.theme}>
              <EventDetailBlock theme={props.theme}>
                {eventDetail.length > 0
                  ? eventDetail.map((item, index) => {
                      return (
                        <EventListItem key={index}>
                          <div>
                            <span className="leftBig">
                              {displayTime(item.starts)}
                            </span>
                            <span className="rightBig">{item.title}</span>
                          </div>
                          <div>
                            <span className="leftBig">
                              {displayDate(item.starts) ===
                              displayDate(item.ends)
                                ? displayTime(item.ends)
                                : `${displayTime(item.ends)} - ${displayDate(
                                    item.ends
                                  )}`}
                            </span>
                            <span className="rightBig"></span>
                          </div>
                        </EventListItem>
                      )
                    })
                  : "No Events"}
              </EventDetailBlock>
            </ModifiedCard>
          </ModifiedCardShield>

          <AddEventsModal
            style={{ top: 20 }}
            width="auto"
            visible={addEventModalVisibility}
            onOk={() => goToAddAnEvent()}
            onCancel={() => setAddEventModalVisibility(false)}
            okText="Add"
            okButtonProps={okBtnState}
            settheme={props.theme}
          >
            <MainTitle title="New Event" marginBottom="20px" />
            <div className="modal-row">
              <Title theme={props.theme}>Title:</Title>
              <StyledInput
                theme={props.theme}
                aria-label="Event title"
                value={title}
                onChange={titleHandleChange}
              />
            </div>
            <div className="modal-row">
              <Title theme={props.theme}>Starts:</Title>
              <DatePicker
                style={{ width: "100%" }}
                disabledDate={disabledStartDate}
                showTime
                format="YYYY-MM-DD HH:mm:ss"
                value={props.startValue}
                placeholder="Start"
                onChange={onStartChange}
              />
            </div>
            <div className="modal-row">
              <Title theme={props.theme}>Ends:</Title>
              <DatePicker
                style={{ width: "100%" }}
                disabledDate={disabledEndDate}
                showTime
                format="YYYY-MM-DD HH:mm:ss"
                value={props.endValue}
                placeholder="End"
                onChange={onEndChange}
              />
            </div>
            <div className="modal-row">
              <Title theme={props.theme}>Repeat:</Title>
              <Select
                value={repeat}
                style={{ width: "100%" }}
                onChange={repeatHandleChange}
              >
                <Select.Option value="never">Never</Select.Option>
                <Select.Option value="Daily">Every Day</Select.Option>
                <Select.Option value="weekly">Every Week</Select.Option>
                <Select.Option value="monthly">Every Month</Select.Option>
                <Select.Option value="yearly">Every Year</Select.Option>
              </Select>
            </div>
          </AddEventsModal>
          <ViewEventsModal
            style={{ top: 20 }}
            width="auto"
            visible={viewEventModalVisibility}
            onCancel={() => setViewEventModalVisibility(false)}
            footer={null}
            settheme={props.theme}
          >
            <MainTitle title="All Events" marginBottom="20px" />
            <div className="modal-row">
              {props.events.length > 0
                ? props.events.sort(compareDate).map((item, index) => {
                    return (
                      <EventListItem key={index} theme={props.theme}>
                        <p className="event-date">{displayDate(item.starts)}</p>
                        <div>
                          <span className="left">
                            {displayTime(item.starts)}
                          </span>
                          <span className="right">{item.title}</span>
                        </div>
                        <div>
                          <span className="left">
                            {displayDate(item.starts) === displayDate(item.ends)
                              ? displayTime(item.ends)
                              : `${displayTime(item.ends)} - ${displayDate(
                                  item.ends
                                )}`}
                          </span>
                          <span className="right"></span>
                        </div>
                      </EventListItem>
                    )
                  })
                : "No Events"}
            </div>
          </ViewEventsModal>
        </Col>
      </MainRow>
    </MainContainer>
  )
}

export default connect(mapStateToProps)(Home)
