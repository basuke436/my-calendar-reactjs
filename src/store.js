import { createStore } from "redux"
import { persistStore, persistReducer } from "redux-persist"
import storage from "redux-persist/lib/storage"

const multipliers = 7
const getUniversalCoordinatedTime = () => {
  const dateString = new Date()
  dateString.setTime(dateString.valueOf() + multipliers * 60 * 60 * 1000)

  return {
    day: dateString.getUTCDay(),
    date: dateString.getUTCDate(),
    month: dateString.getUTCMonth(),
    year: dateString.getUTCFullYear(),
    hour: dateString.getUTCHours(),
    minute: dateString.getUTCMinutes(),
    second: dateString.getUTCSeconds(),
  }
}

const initState = {
  breadcrumb: [],
  utcMultipliers: multipliers,
  newDate: getUniversalCoordinatedTime(),
  theme: "sun",

  selectedDate: new Date(),
  currentMonth: new Date(),
  startValue: null,
  endValue: null,
  events: [],
}

const reducer = (state = initState, action) => {
  switch (action.type) {
    case "SET_BREADCRUMB":
      return {
        ...state,
        breadcrumb: action.data,
      }

    case "FETCH_CLOCK":
      return {
        ...state,
        newDate: getUniversalCoordinatedTime(),
      }

    case "SET_THEME":
      return {
        ...state,
        theme: action.data,
      }

    case "SET_SELECTED_DATE":
      return {
        ...state,
        selectedDate: action.data,
      }

    case "SET_CURRENTMONTH_DATE":
      return {
        ...state,
        currentMonth: action.data,
      }

    case "SET_START_VALUE":
      return {
        ...state,
        startValue: action.data,
      }

    case "SET_END_VALUE":
      return {
        ...state,
        endValue: action.data,
      }

    case "SET_EVENTS":
      return {
        ...state,
        events: action.data,
      }

    default:
      break
  }

  return state
}

const persistConfig = {
  key: "root",
  storage: storage,
  whitelist: ["utcMultipliers", "theme", "events"],
}

const persistedReducer = persistReducer(persistConfig, reducer)

export default () => {
  let store = createStore(persistedReducer)
  let persistor = persistStore(store)
  return { store, persistor }
}
