const version = [
  {
    title: "Jan 19, 2020",
    content: [
      "[Add] compare() for sorting to array object",
      "[Add] notification after add an event",
      "[Add] display events detail block",
      "[Remove] comments",
      "[Edit] code refactoring",
      "[Remove] remove calendar.css out // move it to Calendar and use styled component instead",
      "[Edit] move necessary css from calendar.css to index.css",
      "[Add] display event on calendar if it has some events",
      "[Improve] code refactoring",
      "[Improve] improved some layout",
      "[Edit] change Home to Calendar",
    ],
  },
  {
    title: "Jan 18, 2020",
    content: [
      "[Edit] improved layouts",
      "[Add] OptionBar component",
      "[Remove] some unused importation and variables",
      "[Add] modal for add an event process",
      "[Improve] move getParseFormat() from Calendar component to global function component to reuse from other component",
      "[Improve] set some inherit state to global state that would be use across components",
      "[Add] today button",
      "[Remove] unused an importation",
      "[Add] repeat dropdown",
      "[Improve] improved some layouts",
      "[Improve] code refactoring",
      "[Add] events display block",
      "[Add] save events to local storage",
      "[Add] view event list items modal",
      "[Add] some functions for event timeline display",
      "[Add] getDateAndTimeFormat global function",
    ],
  },
  {
    title: "Jan 17, 2020",
    content: [
      "[Add] install date-fns",
      "[Add] Calendar.js",
      "[Add] calendar.css for display styled calendar",
      "[Add] import Calendar to Home component",
      '[Edit] use "Kanit" google font',
      "[Add] use Icon form antd instead of Material Icons font",
      "[Add] display highlight on today",
      "[Add] border-radius to calendar",
      "[Add] .bg on .today will be displayed as red color",
    ],
  },
  {
    title: "Jan 16, 2020",
    content: ["[Add] initial this project with all necessary environments"],
  },
]

export default version
