After clone this ploject, you have to run:

### `npm install`

Waiting for loading the dependencies from package.json.

### `npm start`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.

You will also see any lint errors in the console.

### A site live link of this project is here:

[https://apirak-calendar.netlify.app/](https://apirak-calendar.netlify.app/)
